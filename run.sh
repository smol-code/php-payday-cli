#!/bin/bash

# Tests
echo "Running tests"
echo
./vendor/bin/phpunit tests

echo
echo "Running CLI with default params (CSV)"
echo
php ./src/main.php

echo
echo "Running CLI with date range and JSON output"
echo
php ./src/main.php --out-format=json --start-date=2012-03 --end-date=2015-04

