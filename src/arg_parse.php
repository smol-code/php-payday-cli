<?php declare(strict_types=1);



namespace salcalc {
	/**
	 * Responsible for handling the CLI input arguments, with defaults
	 * that reflect the given spec.
	 *
	 * Does not quit on invalid input - that's handled by caller.
	 * Does print error messages instead of returning info to caller.
	 * Does not validate that no unknown/unrecognised args were given.
	 */
	class ArgParser
	{
		public string $output_format;
		public \DateTime $start_date;
		public \DateTime $end_date;
		public bool $always_show_year;

		// Raw args given from getopt
		private array $raw_args;

		const AVAILABLE_OUTPUT_FORMATS = array("csv", "json", "jsonugli");
		const DEFAULT_OUTPUT_FORMAT = self::AVAILABLE_OUTPUT_FORMATS[0];

		// Set defaults as CSV, with date range being today to 12 months in the future
		public function __construct(array $cli_args)
		{
			$this->raw_args = $cli_args;

			$this->output_format = self::DEFAULT_OUTPUT_FORMAT;

			// Use "modify()" on these instead of re-constructing them,
			// if they need changing.
			// Personal note... this was weird to write, but I did it like this
			// so the month comparisons were from the start of a month.
			// Otherwise a single-month range couldn't be set, as e.g. 2023-06-01T01:02:03 > 2023-06-01
			$this->start_date = new \DateTime("first day of this month 00:00:00");
			$this->end_date = new \DateTime("first day of this month 00:00:00");
			$this->end_date->modify("+12 month");

			$this->always_show_year = isset($cli_args["always-show-year"]);
		}

		public function want_print_help(): bool
		{
			if (!isset($this->raw_args["help"])) {
				return false;
			}

			print(PHP_EOL . "This tool will produce a CSV file with the dates payments are due, based on the spec. " . PHP_EOL);
			print(PHP_EOL . "By default, the start-date is this month, and the end-date is 12 months in the future. " . PHP_EOL);
			print("If only start-date is changed, the end-date is adjusted to start-date plus 12 months." . PHP_EOL);
			print("If only end-date is changed, the start-date remains as this month. Minimum range is 1 month, max is " . Env::MAX_CALCULATE_MONTHS . PHP_EOL);
			print(PHP_EOL . "Available options with examples: " . PHP_EOL);
			print("  --out-format=csv" . PHP_EOL);
			print("  --start-date=2023-01" . PHP_EOL);
			print("  --end-date=2023-03" . PHP_EOL);
			print("  --always-show-year" . PHP_EOL);
			return true;
		}

		/**
		 * Validates and sets the input args.
		 *
		 * This could've gone in the constructor but when writing
		 * it was easier to toggle like this, and I wanted a bit more control
		 * in case the use-case is somehow expanded.
		 *
		 * returns true/false based on success
		 *
		 * @param  string[] $this->raw_args
		 * @return bool
		 */
		public function validate_set_input_args(): bool
		{
			// TODO: Would be cleaner to use a library for this, but I wanted to keep it simple
			if (!isset($this->raw_args["out-format"])) {
				print("using default out-format: " . self::DEFAULT_OUTPUT_FORMAT . PHP_EOL);
			} elseif (!in_array($this->raw_args["out-format"], self::AVAILABLE_OUTPUT_FORMATS)) {
				print("error: out-format not available, choose from: " . PHP_EOL);
				print_array(self::AVAILABLE_OUTPUT_FORMATS);
				return false;
			} else {
				$this->output_format = $this->raw_args["out-format"];
			}


			if (!isset($this->raw_args["start-date"])) {
				print("using today as start-date" . PHP_EOL);
			} elseif (!validate_date($this->raw_args["start-date"], "Y-m")) {
				print("error: invalid start date format given. please use yyyy-mm" . PHP_EOL);
				return false;
			} else {
				$this->start_date->modify($this->raw_args["start-date"]);
			}


			if (!isset($this->raw_args["end-date"])) {
				print("using 12 months in the future as end-date" . PHP_EOL);
				// This would be cleaner to unconditionally override, since perf
				// doesn't (?) matter and modify() probably isn't expensive.
				// Also wonder if using isset() constantly is slow:
				// https://www.brandonchecketts.com/archives/php-performance-isset-versus-empty-versus-php-notices
				if (isset($this->raw_args["start-date"])) {
					$this->end_date->modify($this->raw_args["start-date"]);
					$this->end_date->modify("+12 month");
				}
			} elseif (!validate_date($this->raw_args["end-date"], "Y-m")) {
				print("error: invalid end date format given. please use yyyy-mm" . PHP_EOL);
				return false;
			} else {
				// Relying on start_date being set beforehand
				$end_date_temp = new \DateTime($this->raw_args["end-date"]);
				$start_end_diff = $this->start_date->diff($end_date_temp);

				// I spent too long trying to get ->format() to convert to total months before
				// I realised there's no way.
				// https://stackoverflow.com/a/15362879
				$start_end_month_diff = $start_end_diff->m + ($start_end_diff->y * 12);

				if ($this->start_date > $end_date_temp) {
					print("error: please don't set end date into the past" . PHP_EOL);
					return false;
				} elseif ($start_end_month_diff > Env::MAX_CALCULATE_MONTHS) {
					print("error: please limit start to end date range to " . Env::MAX_CALCULATE_MONTHS . " months" . PHP_EOL);
					return false;
				} else {
					// NOTE: README1 - The way this is processed, passing just the end-date param,
					// the end-date would not be processed.
					// E.g., setting start-end as 2023-06 to 2023-08 would result in August not being
					// processed.
					// And I think it makes sense for "end-date" to be inclusive of that date.
					$this->end_date->modify($this->raw_args["end-date"]);
					$this->end_date->add(\DateInterval::createFromDateString('1 month'));
				}
			}

			return true;
		}
	}
}
