<?php declare(strict_types=1);



namespace salcalc {

	/**
	 * This is the main class, which handles calculating and outputting the payday.
	 *
	 * Meant to be used in ST. I've not used multithreading/pthreads in PHP and am not microbenchmarking.
	 *
	 * CSV format:
	 * month_name,base_pay_date,bonus_pay_date,year(optional)
	 */
	class PayDay
	{

		// The base and bonus pay dates are processed by separate functions.
		// I was toying with the idea of multithreading for fun, but I decided not to.
		//
		// Format, in the simplest/most concise way I could think of.
		// - $pay_dates = [["year" => "2023", "month" => "May", "base_day" => 31, "bonus_day" => 15]]
		//
		// NOTE: Could/should use a getter but I'm the only "maintainer" so meh
		public array $pay_dates;

		// If there would be a need to add functionality to process actual payment amounts,
		// for multiple employees in a computer-oriented format, they could be split like this.
		// $base = ["2023-04-31" => [
		// 	["name" => "adrian", "amount" => 123],
		// 	["name" => "bob", "amount" => 426],
		// ]];


		// These will be dates including the year.
		// If more than 12 months are being processed, year is output into CSV
		private \DateTime $start_year_month;
		private \DateTime $end_year_month;
		// This is what's actually used for iterating through the months
		private \DatePeriod $month_range;

		// Wanted to re-use these but couldn't use const
		private \DateInterval $INTERVAL_ONE_DAY;
		private \DateInterval $INTERVAL_TWO_DAY;
		private \DateInterval $INTERVAL_FOURTEEN_DAY;
		private \DateInterval $INTERVAL_MONTH;

		public function __construct($p_start, $p_end)
		{
			$this->INTERVAL_ONE_DAY = \DateInterval::createFromDateString('1 day');
			$this->INTERVAL_TWO_DAY = \DateInterval::createFromDateString('2 day');
			$this->INTERVAL_FOURTEEN_DAY = \DateInterval::createFromDateString('14 day');
			$this->INTERVAL_MONTH = \DateInterval::createFromDateString('1 month');

			$this->start_year_month = $p_start;
			$this->end_year_month = $p_end;
			$this->month_range = new \DatePeriod($this->start_year_month, $this->INTERVAL_MONTH, $this->end_year_month);

			// NOTE: Could just call base_salary() and bonus_pay() here
		}

		// This handles setting the years/months, and calculating the
		// base salary pay date for each month.
		public function base_salary()
		{
			// Loop through every month
			$i = 0;
			foreach ($this->month_range as $month) {
				// Don't know of a better way to get the last day of the month...
				// It might make sense to use "last day of this month" for
				// start/end date in ArgParser. But that'd require changes to bonus_pay()
				// NOTE: Name becomes misleading if last day was a weekend,
				// but I wanted to reuse the var and couldn't think of a better name.
				$month_payday_lastday = new \DateTime($month->format("Y-m-t"));

				// Using var here to remove one format call
				$day_str_on_last_day = $month_payday_lastday->format("D");

				// Negate the number of days if it's a weekend
				if ($day_str_on_last_day === "Sun") {
					$month_payday_lastday->sub($this->INTERVAL_TWO_DAY);
				} elseif ($day_str_on_last_day === "Sat") {
					$month_payday_lastday->sub($this->INTERVAL_ONE_DAY);
				}

				// Format: [["year" => "2023", "month" => "May", "base_day" => 31, "bonus_day" => 0]]
				// Done like this so the bonus isn't overwritten if bonus_pay is ran first
				$this->pay_dates[$i]["year"] = $month_payday_lastday->format("Y");
				$this->pay_dates[$i]["month"] = $month_payday_lastday->format("M");
				$this->pay_dates[$i]["base_day"] = $month_payday_lastday->format("d");
				$i++;
			}
		}

		// This only changes the bonus amount in the pay_dates assoc array.
		public function bonus_pay()
		{
			// Paid on 15th, for the previous month.
			// - Not paid on Saturday/Sunday.
			// - If 15th is weekend, pay on the upcoming Wednesday after the 15th
			//
			// (Assumed) That the outputs should always be in the given range
			// - Start month will have "previous" month's bonus pay, rather than a null column.
			// - End month will be same as base_salary end month, not rolling over out of the range.
			// - With this output it would may not be obvious that bonus is for the previous month.


			// Loop through every month
			$i = 0;
			foreach ($this->month_range as $month) {
				// Starting from first date, hence adding 14
				// NOTE: Name becomes misleading if 15th was a weekend,
				// but I wanted to reuse the var and couldn't think of a better name.
				$month_payday_15th = new \DateTime($month->format("Y-m-d"));
				$month_payday_15th->add($this->INTERVAL_FOURTEEN_DAY);

				// Using var here to remove one format call
				$day_str_on_15th = $month_payday_15th->format("D");

				// Skip forward to next Wednesday if 15th was a weekend.
				if ($day_str_on_15th === "Sat" || $day_str_on_15th === "Sun") {
					// I've not worked much with dates in PHP...
					// I can't believe this is so simple
					$month_payday_15th->modify("next Wednesday");
				}

				$this->pay_dates[$i]["bonus_day"] = $month_payday_15th->format("d");
				$i++;
			}
		}
	}

}
