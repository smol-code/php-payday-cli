<?php declare(strict_types=1);

require_once(__DIR__ . "/../src/env.php");
require_once(__DIR__ . "/../src/arg_parse.php");


use PHPUnit\Framework\TestCase;

final class arg_parse_test extends TestCase
{

	public function test_invalid_out_format(): void
	{
		$arg_parser = new \salcalc\ArgParser(["out-format" => "JSON_was_gonna_be_called_JSML"]);
		$arg_parser->validate_set_input_args();
		// Using regex so things like adding more output format options doesn't break this test
		$this->expectOutputRegex("/.*out-format\ not\ available.*/");
	}

	public function test_end_date_back_to_the_past(): void
	{
		$expected_start_date = new DateTime("2023-04-01 00:00:00");

		$cli_start_date = "2023-04";
		$cli_end_date = "2022-03";

		$arg_parser = new \salcalc\ArgParser(["start-date" => $cli_start_date, "end-date" => $cli_end_date]);
		$arg_parser->validate_set_input_args();
		// I am once again apologising for using regex
		$this->expectOutputRegex("/.*please don't set end date into the past.*/");
	}

	// Test default 12month in future
	public function test_default_dates(): void
	{
		// Note that this can technically fail if you test right as the day ticks over to the next month.
		$default_start_date = new DateTime("first day of this month 00:00:00");
		$default_end_date = clone $default_start_date;
		// Testing using add, whereas code uses modify(). Just cause why not
		$default_end_date->add(DateInterval::createFromDateString('12 month'));

		$arg_parser = new \salcalc\ArgParser([]);

		$this->assertSame($default_start_date->format("Y-m-d"), $arg_parser->start_date->format("Y-m-d"));
		$this->assertSame($default_end_date->format("Y-m-d"), $arg_parser->end_date->format("Y-m-d"));
	}

	public function test_start_date_only(): void
	{
		$expected_start_date = new DateTime("2023-03-01 00:00:00");
		$expected_end_date = new DateTime("2024-03-01 00:00:00");

		$cli_start_date = "2023-03";

		$arg_parser = new \salcalc\ArgParser(["start-date" => $cli_start_date]);
		$arg_parser->validate_set_input_args();

		$this->assertSame($expected_start_date->format("Y-m-d"), $arg_parser->start_date->format("Y-m-d"));
		$this->assertSame($expected_end_date->format("Y-m-d"), $arg_parser->end_date->format("Y-m-d"));
	}

	public function test_end_date_only(): void
	{
		$expected_start_date = new DateTime("first day of this month 00:00:00");
		// Making end date relative so that the test date isn't hardcoded and won't need maintenance
		$expected_end_date = clone $expected_start_date;
		$expected_end_date->modify("+6 month");

		// And CLI end date is one month negative, see below NOTE or README.md about the offset.
		$cli_end_date_datetime = clone $expected_end_date;
		$cli_end_date_datetime->modify("-1 month");
		$cli_end_date = $cli_end_date_datetime->format("Y-m");

		$arg_parser = new \salcalc\ArgParser(["end-date" => $cli_end_date]);
		$arg_parser->validate_set_input_args();

		$this->assertSame($expected_start_date->format("Y-m-d"), $arg_parser->start_date->format("Y-m-d"));
		$this->assertSame($expected_end_date->format("Y-m-d"), $arg_parser->end_date->format("Y-m-d"));
	}

	public function test_start_end_date_explicit(): void
	{
		$expected_start_date = new DateTime("2023-04-01 00:00:00");
		// NOTE: Correct behaviour that this is bumped up by one month vs cli_end_date, as I chose to make the end-date's month
		// be inclusive. So when it calculates it includes August in the output.
		$expected_end_date = new DateTime("2023-08-01 00:00:00");

		$cli_start_date = "2023-04";
		$cli_end_date = "2023-07";

		$arg_parser = new \salcalc\ArgParser(["start-date" => $cli_start_date, "end-date" => $cli_end_date]);
		$arg_parser->validate_set_input_args();

		$this->assertSame($expected_start_date->format("Y-m-d"), $arg_parser->start_date->format("Y-m-d"));
		$this->assertSame($expected_end_date->format("Y-m-d"), $arg_parser->end_date->format("Y-m-d"));
	}


}

