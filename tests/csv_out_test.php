<?php declare(strict_types=1);

require_once(__DIR__ . "/../src/pay_day.php");
require_once(__DIR__ . "/../src/csv_output.php");


use PHPUnit\Framework\TestCase;

final class csv_out_test extends TestCase
{
	private $tmp_dir_checked_exists;

	public function __constructor()
	{
		$this->tmp_dir_checked_exists = false;
	}

	// Used to avoid hand-crafting pay_dates given to CsvOut.
	// Copied from: 
	// - arg_parse_test.php > test_start_end_date_exlicit()
	// - pay_day_test.php > test_base_and_bonus...()
	//
	// Kept tests here just in case
	//
	// Allowing year-month to be changed to trigger the "more than 12 month defaults to show year" test
	private function setup_payday_dates($p_start_yyyy_mm = "2023-04", $p_end_yyyy_mm = "2023-08"): array
	{
		$expected_start_date = new DateTime($p_start_yyyy_mm . "-01 00:00:00");
		$expected_end_date = new DateTime($p_end_yyyy_mm . "-01 00:00:00");
		
		// Again... gotta ofset cli end date to be -1 that of what's internal in ArgParser
		$cli_end_date_datetime = clone $expected_end_date;
		$cli_end_date_datetime->modify("-1 month");

		$cli_start_date = $p_start_yyyy_mm;
		$cli_end_date = $cli_end_date_datetime->format("Y-m");

		$arg_parser = new \salcalc\ArgParser(["start-date" => $cli_start_date, "end-date" => $cli_end_date]);
		$arg_parser->validate_set_input_args();

		$this->assertSame($expected_start_date->format("Y-m-d"), $arg_parser->start_date->format("Y-m-d"));
		$this->assertSame($expected_end_date->format("Y-m-d"), $arg_parser->end_date->format("Y-m-d"));

		$pay_day_calc = new \salcalc\PayDay($expected_start_date, $expected_end_date);
		$pay_day_calc->base_salary();
		$pay_day_calc->bonus_pay();
		return $pay_day_calc->pay_dates;
	}

	private function gen_temp_csv_name(): string
	{
		if (!$this->tmp_dir_checked_exists) {
			$this->tmp_dir_checked_exists = true;
			if (!is_dir("./test_tmp/")) {
				mkdir("./test_tmp/");
			}
		}
		// Using unix timestamp + rand to make output files (hopefully) unique
		// in case they run at the same time.
		// In real code I'd probably look for a better solution, and with
		// single-threaded I think it could just check if file exists and increment a suffix.
		return "./test_tmp/csv_out_test" . time() . "_" . rand(10000, 99999);
	}

	public function test_default_year_hidden(): void
	{
		$pay_dates = $this->setup_payday_dates();
		$temp_csv_path = $this->gen_temp_csv_name();

		$csv_writer = new \salcalc\CsvOut($pay_dates, false, $temp_csv_path);
		$csv_writer->write_file();

		$csv_reader = \League\Csv\Reader::createFromPath($temp_csv_path, "r");
		$this->assertSame(
			"month_name,base_pay_date,bonus_pay_date\n" .
			"Apr,28,19\n" .
			"May,31,15\n" .
			"Jun,30,15\n" .
			"Jul,31,19\n",
			$csv_reader->toString()
		);
	}

	public function test_always_year(): void
	{
		$pay_dates = $this->setup_payday_dates();
		$temp_csv_path = $this->gen_temp_csv_name();

		$csv_writer = new \salcalc\CsvOut($pay_dates, true, $temp_csv_path);
		$csv_writer->write_file();

		$csv_reader = \League\Csv\Reader::createFromPath($temp_csv_path, "r");
		$this->assertSame(
			"month_name,base_pay_date,bonus_pay_date,year\n" .
			"Apr,28,19,2023\n" .
			"May,31,15,2023\n" .
			"Jun,30,15,2023\n" .
			"Jul,31,19,2023\n",
			$csv_reader->toString()
		);
	}

	public function test_default_year_shown_over_12_months(): void
	{
		$pay_dates = $this->setup_payday_dates("2023-01", "2024-02");
		$temp_csv_path = $this->gen_temp_csv_name();

		$csv_writer = new \salcalc\CsvOut($pay_dates, false, $temp_csv_path);
		$csv_writer->write_file();

		$csv_reader = \League\Csv\Reader::createFromPath($temp_csv_path, "r");
		$this->assertSame(
			"month_name,base_pay_date,bonus_pay_date,year\n" .
			"Jan,31,18,2023\n" .
			"Feb,28,15,2023\n" .
			"Mar,31,15,2023\n" .
			"Apr,28,19,2023\n" .
			"May,31,15,2023\n" .
			"Jun,30,15,2023\n" .
			"Jul,31,19,2023\n" .
			"Aug,31,15,2023\n" .
			"Sep,29,15,2023\n" .
			"Oct,31,18,2023\n" .
			"Nov,30,15,2023\n" .
			"Dec,29,15,2023\n" .
			"Jan,31,15,2024\n",
			$csv_reader->toString()
		);
	}

	public function test_bonus_pay(): void
	{
		$start_date = new DateTime("2023-07-01 00:00:00");
		$end_date = new DateTime("2023-10-01 00:00:00");

		$pay_day_calc = new \salcalc\PayDay($start_date, $end_date);

		$pay_day_calc->bonus_pay();
		$this->assertSame([
			[
				"bonus_day" => "19"
			],
			[
				"bonus_day" => "15"
			],
			[
				"bonus_day" => "15"
			],
		], $pay_day_calc->pay_dates);
	}

	// Assure that calls to base_salary and bonus_pay can happen in either order,
	// and that they can happen multiple times with the same end result.
	public function test_base_and_bonus_order_nonsensitive(): void
	{
		$start_date = new DateTime("2023-07-01 00:00:00");
		$end_date = new DateTime("2023-10-01 00:00:00");

		$pay_day_calc = new \salcalc\PayDay($start_date, $end_date);

		$pay_day_calc->base_salary();
		$pay_day_calc->bonus_pay();
		$this->assertSame([
			[
				"year" => "2023",
				"month" => "Jul",
				"base_day" => "31",
				"bonus_day" => "19"
			],
			[
				"year" => "2023",
				"month" => "Aug",
				"base_day" => "31",
				"bonus_day" => "15"
			],
			[
				"year" => "2023",
				"month" => "Sep",
				"base_day" => "29",
				"bonus_day" => "15"
			],
		], $pay_day_calc->pay_dates);

		$pay_day_calc->base_salary();
		$this->assertSame([
			[
				"year" => "2023",
				"month" => "Jul",
				"base_day" => "31",
				"bonus_day" => "19"
			],
			[
				"year" => "2023",
				"month" => "Aug",
				"base_day" => "31",
				"bonus_day" => "15"
			],
			[
				"year" => "2023",
				"month" => "Sep",
				"base_day" => "29",
				"bonus_day" => "15"
			],
		], $pay_day_calc->pay_dates);

		$pay_day_calc->bonus_pay();
		$this->assertSame([
			[
				"year" => "2023",
				"month" => "Jul",
				"base_day" => "31",
				"bonus_day" => "19"
			],
			[
				"year" => "2023",
				"month" => "Aug",
				"base_day" => "31",
				"bonus_day" => "15"
			],
			[
				"year" => "2023",
				"month" => "Sep",
				"base_day" => "29",
				"bonus_day" => "15"
			],
		], $pay_day_calc->pay_dates);

	}

}

