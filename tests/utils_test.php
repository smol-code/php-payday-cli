<?php declare(strict_types=1);

require_once(__DIR__ . "/../src/utils.php");

// require __DIR__ . "/../src/utils.php";

use PHPUnit\Framework\TestCase;

final class utils_test extends TestCase
{
	public function test_validate_date_default_format(): void
	{
		$date_str = '2021-03-04';
		$is_valid = validate_date($date_str);

		$this->assertSame(true, $is_valid);
	}

	public function test_validate_default_format_invalid(): void
	{
		$date_str = '2021-03';
		$is_valid = validate_date($date_str);

		$this->assertSame(false, $is_valid);
	}

	public function test_validate_custom_format(): void
	{
		$date_str = '2021-03';
		$is_valid = validate_date($date_str, "Y-m");

		$this->assertSame(true, $is_valid);
	}

	public function test_validate_custom_format_invalid(): void
	{
		$date_str = '2021-03-04';
		$is_valid = validate_date($date_str, "Y-m");

		$this->assertSame(false, $is_valid);
	}



	public function test_print_array(): void
	{
		$arr = [
			"a" => 1,
			"b" => 3,
			"c" => 5,
			"d" => 7
		];
		print_array($arr);

		$this->expectOutputString(
			" - 1" . PHP_EOL .
			" - 3" . PHP_EOL .
			" - 5" . PHP_EOL .
			" - 7" . PHP_EOL
		);
	}
}

