<?php declare(strict_types=1);

require_once(__DIR__ . "/../src/pay_day.php");


use PHPUnit\Framework\TestCase;

final class pay_day_test extends TestCase
{
	public function test_base_salary(): void
	{
		// Note that end-date excludes the last month.
		// This is inclusive in the CLI because the ArgParser adds one month to end-date.
		$start_date = new DateTime("2023-07-01 00:00:00");
		$end_date = new DateTime("2023-10-01 00:00:00");

		$pay_day_calc = new \salcalc\PayDay($start_date, $end_date);

		$pay_day_calc->base_salary();
		$this->assertSame([
			[
				"year" => "2023",
				"month" => "Jul",
				"base_day" => "31"
			],
			[
				"year" => "2023",
				"month" => "Aug",
				"base_day" => "31"
			],
			[
				"year" => "2023",
				"month" => "Sep",
				"base_day" => "29"
			],
		], $pay_day_calc->pay_dates);
	}

	public function test_bonus_pay(): void
	{
		$start_date = new DateTime("2023-07-01 00:00:00");
		$end_date = new DateTime("2023-10-01 00:00:00");

		$pay_day_calc = new \salcalc\PayDay($start_date, $end_date);

		$pay_day_calc->bonus_pay();
		$this->assertSame([
			[
				"bonus_day" => "19"
			],
			[
				"bonus_day" => "15"
			],
			[
				"bonus_day" => "15"
			],
		], $pay_day_calc->pay_dates);
	}

	// Assure that calls to base_salary and bonus_pay can happen in either order,
	// and that they can happen multiple times with the same end result.
	public function test_base_and_bonus_order_nonsensitive(): void
	{
		$start_date = new DateTime("2023-07-01 00:00:00");
		$end_date = new DateTime("2023-10-01 00:00:00");

		$pay_day_calc = new \salcalc\PayDay($start_date, $end_date);

		$pay_day_calc->base_salary();
		$pay_day_calc->bonus_pay();
		$this->assertSame([
			[
				"year" => "2023",
				"month" => "Jul",
				"base_day" => "31",
				"bonus_day" => "19"
			],
			[
				"year" => "2023",
				"month" => "Aug",
				"base_day" => "31",
				"bonus_day" => "15"
			],
			[
				"year" => "2023",
				"month" => "Sep",
				"base_day" => "29",
				"bonus_day" => "15"
			],
		], $pay_day_calc->pay_dates);

		$pay_day_calc->base_salary();
		$this->assertSame([
			[
				"year" => "2023",
				"month" => "Jul",
				"base_day" => "31",
				"bonus_day" => "19"
			],
			[
				"year" => "2023",
				"month" => "Aug",
				"base_day" => "31",
				"bonus_day" => "15"
			],
			[
				"year" => "2023",
				"month" => "Sep",
				"base_day" => "29",
				"bonus_day" => "15"
			],
		], $pay_day_calc->pay_dates);

		$pay_day_calc->bonus_pay();
		$this->assertSame([
			[
				"year" => "2023",
				"month" => "Jul",
				"base_day" => "31",
				"bonus_day" => "19"
			],
			[
				"year" => "2023",
				"month" => "Aug",
				"base_day" => "31",
				"bonus_day" => "15"
			],
			[
				"year" => "2023",
				"month" => "Sep",
				"base_day" => "29",
				"bonus_day" => "15"
			],
		], $pay_day_calc->pay_dates);

	}

}

