<?php declare(strict_types=1);



namespace salcalc {
		
	/**
	 * Constants and env-style variables go here.
	 */
	class Env
	{
		const PRINT_DEBUG=false;
		const MAX_CALCULATE_MONTHS=48;
	};
}
