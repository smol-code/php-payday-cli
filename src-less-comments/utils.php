<?php declare(strict_types=1);



/**
 * Print items of the array in an indented list.
 *
 * @param  array $arr
 * @return void
 */
function print_array(array $arr)
{
	foreach ($arr as $elem) {
		echo " - " . $elem . PHP_EOL;
	}
}



/**
 * Validate the given date string matches the provided format.
 *
 * Input dates (at time of writing) are yyyy-mm, but I wanted
 * to keep the default format param as something sane.
 *
 * See: https://stackoverflow.com/a/19271434
 *
 * @param string $given_date
 * @param string $format
 * @return boolean
 */
function validate_date($given_date, $format = 'Y-m-d')
{
	$date = DateTime::createFromFormat($format, $given_date);
	return $date && $date->format($format) === $given_date;
}
