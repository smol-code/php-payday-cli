<?php declare(strict_types=1);



namespace salcalc {

	/**
	 * Handles outputting the base and bonus pay dates into JSON
	 *
	 * This is a uch simpler JSON version of CsvOut, with stdlib json_encode and file_put_contents.
	 *
	 * Comments removed as they're redundant
	 */
	class JsonOut
	{
		private array $pay_dates;
		private string $file_path;
		private bool $beautify_output;
		public function __construct($p_pay_dates, $p_out_file, $p_beautify_output)
		{
			if (file_exists($p_out_file)) {
				print($p_out_file . " already exists. will replace" . PHP_EOL);
			}
			$this->file_path = $p_out_file;
			$this->pay_dates = $p_pay_dates;
			$this->beautify_output = $p_beautify_output;
		}

		public function write_file()
		{
			$organised_data = [];
			foreach ($this->pay_dates as $pay_date) {
				$organised_data[$pay_date["year"]][$pay_date["month"]] = [
					"base_day" => $pay_date["base_day"],
					"bonus_day" => $pay_date["bonus_day"]
				];
			}
			$json_data = ($this->beautify_output ? json_encode($organised_data, JSON_PRETTY_PRINT) : json_encode($organised_data));
			file_put_contents($this->file_path, $json_data);
		}
	}
}
