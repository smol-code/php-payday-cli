<?php declare(strict_types=1);



namespace salcalc {

	use Exception;
	use League\Csv\UnavailableStream;

	/**
	 * Handles outputting the base and bonus pay dates into CSV
	 *
	 * Will output an extra year field if outputting more than 12 months,
	 * or if user set the option.
	 *
	 * See: https://csv.thephpleague.com/9.0/writer/
	 * > When inserting records into a CSV document using League\Csv\Writer,
	 * > first insert all the data that need to be inserted before starting manipulating the CSV.
	 * > If you manipulate your CSV document before insertion, you may change
	 * > the file cursor position and erase your data.
	 *
	 * TODO: Caller should control exit instead of the the catch statements but I'm being lazy.
	 */
	class CsvOut
	{

		private array $pay_dates;
		private \League\Csv\Writer $csv_writer;
		private array $first_line;
		private bool $show_year_col;

		public function __construct($p_pay_dates, $always_show_year, $p_out_file)
		{
			if (file_exists($p_out_file)) {
				print($p_out_file . " already exists. will replace" . PHP_EOL);
			}

			try {
				$this->csv_writer = \League\Csv\Writer::createFromPath($p_out_file, "w+");
			} catch (UnavailableStream $e) {
				print(PHP_EOL);
				print("Cannot output to file ./output.csv" . PHP_EOL);
				print("There could be a lockfile on the file" . PHP_EOL);
				print(PHP_EOL);
				print("Exception details: " . PHP_EOL);
				print(PHP_EOL);
				print($e);
				print(PHP_EOL);
				exit;
			} catch (Exception $e) {
				print(PHP_EOL);
				print("Unknown error occured when creating ./output.csv" . PHP_EOL);
				print("Exception details: " . PHP_EOL);
				print(PHP_EOL);
				print($e);
				print(PHP_EOL);
				exit;
			}

			$this->pay_dates = $p_pay_dates;

			if ($always_show_year || count($p_pay_dates) > 12) {
				print("outputting CSV with additional year column" . PHP_EOL);
				$this->first_line = ["month_name","base_pay_date","bonus_pay_date", "year"];
				$this->show_year_col = true;
			} else {
				$this->first_line = ["month_name","base_pay_date","bonus_pay_date"];
				$this->show_year_col = false;
			}
		}

		public function write_file()
		{
			try {
				$this->csv_writer->insertOne($this->first_line);

				if ($this->show_year_col) {
					foreach ($this->pay_dates as $pay_date) {
						$this->csv_writer->insertOne([
							$pay_date["month"],
							$pay_date["base_day"],
							$pay_date["bonus_day"],
							$pay_date["year"]
						]);
					}
				} else {
					foreach ($this->pay_dates as $pay_date) {
						$this->csv_writer->insertOne([
							$pay_date["month"],
							$pay_date["base_day"],
							$pay_date["bonus_day"]
						]);
					}
				}
			} catch (Exception $e) {
				print(PHP_EOL);
				print("Unknown error occured when wriing rows to ./output.csv" . PHP_EOL);
				print("Exception details: " . PHP_EOL);
				print(PHP_EOL);
				print($e);
				print(PHP_EOL);
				exit;
			}
		}
	}

}
