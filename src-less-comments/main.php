<?php declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';

require_once("./src/utils.php");
require_once("./src/env.php");
require_once("./src/arg_parse.php");
require_once("./src/pay_day.php");
require_once("./src/csv_output.php");
require_once("./src/json_output.php");



// Entry point
if (isset($argc)) {
	$opts = getopt("", [
		"help",
		"always-show-year",
		"out-format:",
		"start-date:",
		"end-date:"
	]);


	$arg_parse = new \salcalc\ArgParser($opts);

	if ($arg_parse->want_print_help()) {
		return;
	}

	if (!$arg_parse->validate_set_input_args()) {
		print("exiting..." . PHP_EOL);
		return;
	}

	if (\salcalc\Env::PRINT_DEBUG) {
		print(PHP_EOL);
		print("Parsed arguments:" . PHP_EOL);
		var_dump($arg_parse);
		print(PHP_EOL);
	}


	$pay_day_calc = new \salcalc\PayDay($arg_parse->start_date, $arg_parse->end_date);

	$pay_day_calc->base_salary();
	$pay_day_calc->bonus_pay();

	if (\salcalc\Env::PRINT_DEBUG) {
		print(PHP_EOL);
		print("Calculated pay dates:" . PHP_EOL);
		var_dump($pay_day_calc->pay_dates);
		print(PHP_EOL);
	}


	if ($arg_parse->output_format === "csv") {
		$csv_writer = new \salcalc\CsvOut($pay_day_calc->pay_dates, $arg_parse->always_show_year, "./output.csv");
		$csv_writer->write_file();
	} else {
		// "beautify" if format is json, not jsonugli
		$do_beautify = $arg_parse->output_format === "json";
		$json_writer = new \salcalc\JsonOut($pay_day_calc->pay_dates, "./output.json", $do_beautify);
		$json_writer->write_file();
	}
}
