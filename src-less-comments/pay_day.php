<?php declare(strict_types=1);



namespace salcalc {

	/**
	 * This is the main class, which handles calculating and outputting the payday.
	 *
	 * CSV format:
	 * month_name,base_pay_date,bonus_pay_date,year(optional)
	 */
	class PayDay
	{

		// Format:
		// - $pay_dates = [["year" => "2023", "month" => "May", "base_day" => 31, "bonus_day" => 15]]
		public array $pay_dates;

		private \DateTime $start_year_month;
		private \DateTime $end_year_month;
		private \DatePeriod $month_range;

		private \DateInterval $INTERVAL_ONE_DAY;
		private \DateInterval $INTERVAL_TWO_DAY;
		private \DateInterval $INTERVAL_FOURTEEN_DAY;
		private \DateInterval $INTERVAL_MONTH;

		public function __construct($p_start, $p_end)
		{
			$this->INTERVAL_ONE_DAY = \DateInterval::createFromDateString('1 day');
			$this->INTERVAL_TWO_DAY = \DateInterval::createFromDateString('2 day');
			$this->INTERVAL_FOURTEEN_DAY = \DateInterval::createFromDateString('14 day');
			$this->INTERVAL_MONTH = \DateInterval::createFromDateString('1 month');

			$this->start_year_month = $p_start;
			$this->end_year_month = $p_end;
			$this->month_range = new \DatePeriod($this->start_year_month, $this->INTERVAL_MONTH, $this->end_year_month);
		}

		// This handles setting the years/months, and calculating the
		// base salary pay date for each month.
		public function base_salary()
		{
			$i = 0;
			foreach ($this->month_range as $month) {
				$month_payday_lastday = new \DateTime($month->format("Y-m-t"));

				$day_str_on_last_day = $month_payday_lastday->format("D");

				if ($day_str_on_last_day === "Sun") {
					$month_payday_lastday->sub($this->INTERVAL_TWO_DAY);
				} elseif ($day_str_on_last_day === "Sat") {
					$month_payday_lastday->sub($this->INTERVAL_ONE_DAY);
				}

				// Done like this so the bonus isn't overwritten if bonus_pay is ran first
				$this->pay_dates[$i]["year"] = $month_payday_lastday->format("Y");
				$this->pay_dates[$i]["month"] = $month_payday_lastday->format("M");
				$this->pay_dates[$i]["base_day"] = $month_payday_lastday->format("d");
				$i++;
			}
		}

		// This only changes the bonus amount in the pay_dates assoc array.
		public function bonus_pay()
		{
			$i = 0;
			foreach ($this->month_range as $month) {
				// Starting from first date, hence adding 14
				// NOTE: Name becomes misleading if 15th was a weekend,
				// but I wanted to reuse the var and couldn't think of a better name.
				$month_payday_15th = new \DateTime($month->format("Y-m-d"));
				$month_payday_15th->add($this->INTERVAL_FOURTEEN_DAY);

				$day_str_on_15th = $month_payday_15th->format("D");

				// Skip forward to next Wednesday if 15th was a weekend.
				if ($day_str_on_15th === "Sat" || $day_str_on_15th === "Sun") {
					$month_payday_15th->modify("next Wednesday");
				}

				$this->pay_dates[$i]["bonus_day"] = $month_payday_15th->format("d");
				$i++;
			}
		}
	}

}
