# payday-cli
This script outputs the dates on which employees shall be paid.
- If the last day is not a weekend, pay base salary on the last day.
- - Else pay on the latest (within the same month) week-day.
- Bonus salaries are paid on the 15th, unless it is a weekend.
- - If it's a weekend, pay on the following Wednesday.


See `./src/` for the main source code, with a bunch of extra comments that I often wouldn't leave.
- `./src/main.php` has the entry point.
- `./src-less-comments` has a few comments stripped out and may be easier to read. There are minimal "self-reflective" and todo-if-I-gave-myself-more-time comments.



<br>



## Known issues etc.
- Invalid CLI arguments do not cause warning or error.
- Inputting one argument multiple times will cause errors, mostly TypeErrors and confusing stdout for `out-format`.



<br>



## Tooling and Libraries
- Composer
- phpcs + phpcbf + PSR2
- league/csv
- PHPUnit
- GitLab CI/CD
- Written locall in PHP 8.2.7 but probably without fully exploiting the type system



<br>



## Self notes
I would've liked to toy with putting the script behind NGINX for fun, and making ArgParser read HTTP params, but that would probably massively clutter the code and be pointless for this exercise.


I split the `PayDay->base_salary()` and `PayDay->bonus_pay()` functions to play around with multithreading but none of that made it into the code. I think it makes the code a little cleaner and gives the option of not calling `bonus_pay()` (though in that case another conditional would be needed in CsvOut).


I'm not sure if Doxygen is the go-to in industry, but that's how I would've generated documentation.
There's so little code though, and it's not commented all that well, so I didn't both.

I should've probably used getters more instead of exposing public vars.



<br>



## Testing
Using PHPUnit.

I couldn't really find the time to write thorough unit tests but I included some.

Some small sections of the tests combine multiple operations, e.g. `ArgParser` -> `PayDay` -> `CsvOut` in `./tests/csv_out_test.php`.



<br>



## Code style notes
I'm basically just using PSR2 with LF line endings and tabs. It's deprecated but I've not looked into PSR12.

Please excuse the discrepancy in the tests and the main files.

phpcs wanted PascalCase but I kept my file name snake_case, which meant PHPUnit wanted a 1:1 match of the file name to the class name.



<br>



## Params
The default month range is 12 months, starting with this month.

The maximum range is set to 48 months - just arbitrarily, but in practice it can be higher.

**The `start-date` and `end-date` may be misleading as they take `yyyy-mm` and no day.**



### help
- `--help`

Basic help output.



### out-format
- Default: `csv`
- E.g., `--out-format=csv`

Options:
- `csv`
- `json` - uses JSON_PRETTY_PRINT
- `jsonugli` - minimal filesize

The CSV output is formatted, as I understand it, as it should be. However, I decided to make the JSON output more hierarchical and arguably more computer-readable.



### start-date
- Default: Today's month
- E.g., `--start-date=2023-05`

If no start-date is provided, the current month will be processed.

If a start-date is provided, the first month processed will be inclusive of the given month.



### end-date
- Default: Today +12 months
- E.g., `--end-date=2024-06`


If no `end-date` is provided, twelve months will be processed based on the start month.
- Starting on 2023-06, the final month will be 2024-05.


If an `end-date` is provided, the months up to and including the month will be processed.
- `end-date` of 2024-06 will include July in the output.
- This wasn't the initial or default behaviour, but I think this makes sense.
- For code reference, grep for string `README1` in `.src/arg_parse.php`.


The `end-date` cannot be in the past relative to the start month.
- Hence the `start-date` must be provided if you want to time travel.



### always-show-year
- `--always-show-year`

Provide this argument (without a value) to always output the year on the right-side of the CSV file.

The year will, by default, only be output if there are more than 12 months in the output.



<br>



## Env
See `./src/env.php`

This file is a combination of some constants and what would typically be "env" variables.

_Well, there's only two things there but..._



### PRINT_DEBUG
- Default: `false`

Set this to true to have some `var_dump` logs output in the console.

Usually I'd use a helper function or preprocessor definitions but I didn't see a need for much logging.



### MAX_CALCULATE_MONTHS
- Default: `48`

This is the max number of months that the `start-date` to `end-date` range can specify.

